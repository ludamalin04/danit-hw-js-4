'use strict'
let num1;
let num2;
let operator;

enterData();
calcData();



function enterData() {
    num1 = +prompt('Enter first number', num1);
    num2 = +prompt('Enter second number', num2);
    operator = prompt('Enter operator: + - * /');
}

function checkData(number) {
    if (isNaN(number) || number === null || number === '') {
        return null;
    } else
        return number;
}

function calcData() {
    if (checkData(num1) && checkData(num2)) {
        switch (operator) {
            case '+':
                console.log(num1 + num2);
                break;
            case '-':
                console.log(num1 - num2);
                break;
            case '*':
                console.log(num1 * num2);
                break;
            case '/':
                console.log(num1 / num2);
                break;
            default:
                console.log("No value found");
        }
    } else {
        enterData();
        calcData()
    }
}